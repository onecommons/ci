import time
import docker
import os
import asyncio
import json
import urllib
from gitlab import Gitlab
from unfurl.localenv import LocalEnv
from unfurl.to_json import get_deploymentpaths
from unfurl.server import update_deployment
from functools import cached_property

ALLOWED_PARALLEL = [
    "failed",
    "skipped",
    "success",
    "canceled",
    "scheduled",
    "manual"
]


docker_client = None
docker_client_error = None

for retry in range(10):
    try:
        docker_client = docker.from_env()
        docker_client_error = None
        break
    except Exception as e:
        docker_client_error = e
        time.sleep((retry + 1))

if docker_client_error is not None:
    raise docker_client_error

unfurl_project = LocalEnv(can_be_empty=True).project
assert unfurl_project


class UnfurlCloudDeployment:
    def __init__(self, key, deployment_record):
        self.key = key

        if not deployment_record:
            raise Exception(f"Expected deployment record for {key}")

        self.deployment_record = deployment_record

        CI_PROJECT_ID = int(os.getenv("CI_PROJECT_ID"))
        CI_SERVER_URL = os.getenv("CI_SERVER_URL")
        ACCESS_TOKEN = os.getenv("UNFURL_PROJECT_TOKEN")
        gitlab = Gitlab(CI_SERVER_URL, private_token=ACCESS_TOKEN)
        self.project = gitlab.projects.get(CI_PROJECT_ID)

    def update_record_with_upstream(
        self,
        pipeline_attributes,
        dispatched_by,
        upstream_pipeline,
        upstream_branch,
        upstream_commit,
    ):
        pipeline_id = pipeline_attributes.get("id")
        commit_id = pipeline_attributes.get("sha")
        variables_map = self.latest_pipeline.variables_map
        inferred_project_id = (
            self.latest_pipeline.upstream_project_id
        )  # TODO assert this is correct

        new_pipeline_record = {
            "id": pipeline_id,
            "commit_id": commit_id,
            "variables": variables_map,
            "dispatched_by": dispatched_by,
            "upstream_commit_id": upstream_commit,
            "upstream_pipeline_id": upstream_pipeline,
            "upstream_branch": upstream_branch,
            "upstream_project_id": inferred_project_id,
        }

        self.deployment_record["pipelines"].append(new_pipeline_record)
        update_deployment(unfurl_project, self.key,
                          self.deployment_record, True)

    def trigger_incremental_deployment(
        self,
        project,
        dispatched_by,
        upstream_pipeline,
        upstream_branch,
        upstream_commit,
    ):
        print(
            f"triggering incremental deployment for {self.key=} {project=} {dispatched_by=} {upstream_pipeline=} {upstream_branch=} {upstream_commit=}"
        )
        if not self.can_incremental_deploy:
            return None

        trigger_variables = []
        for entry in self.latest_pipeline.variables:
            if entry["key"] == "UPSTREAM_COMMIT":
                trigger_variables.append(
                    {"key": "UPSTREAM_COMMIT", "value": upstream_commit}
                )
            else:
                trigger_variables.append(entry)

        print(f"{trigger_variables=}")
        pipeline = project.pipelines.create(
            {"ref": "main", "variables": trigger_variables}
        )
        pipeline_id = pipeline.attributes.get("id")
        self.update_record_with_upstream(
            pipeline.attributes,
            dispatched_by,
            upstream_pipeline,
            upstream_branch,
            upstream_commit,
        )
        return pipeline_id

    @property
    def incremental_deploy_enabled(self):
        result = self.deployment_record.get("incremental_deploy")
        assert result in [True, False, None]

        return bool(result)

    @property
    def can_incremental_deploy(self):
        if self.latest_pipeline is None:
            print("cannot incremental deploy because pipeline is none")
            return False
        if not self.incremental_deploy_enabled:
            print("incremental deploy disabled")
            return False
        if self.latest_pipeline.workflow == "undeploy":
            print("was teared down")
            return False

        pipeline = self.project.pipelines.get(self.latest_pipeline.id)
        if pipeline.attributes.get("status") != "success":
            print("Skipping incremental deploy because CI pipeline was not a success")

            blueprint_base_url = self.latest_pipeline.blueprint_project_url.replace(
                ".git", ""
            )
            dashboard_path = urllib.parse.quote(
                os.getenv("CI_PROJECT_PATH"), safe="")
            print(
                f"Deploy anyway: {blueprint_base_url}/deployment-drafts/{dashboard_path}/{self.latest_pipeline.deploy_environment}/{self.latest_pipeline.deployment}"
            )

            return False

        return True

    def should_incremental_deploy(self, project_id=None, branch=None, commit=None):
        if not self.can_incremental_deploy:
            return False

        if project_id is None or branch is None:
            return True

        try:
            subscriptions = json.loads(os.getenv("UNFURL_PROJECT_SUBSCRIPTIONS"))[
                "subscriptions"
            ]
            path = os.getenv("UPSTREAM_REPO").lower()
            subscriptions = subscriptions[path]
            found = False

            for subscription in subscriptions:
                if (
                    subscription["deploymentName"] == self.latest_pipeline.deployment
                    and subscription["environmentName"]
                    == self.latest_pipeline.deploy_environment
                ):
                    found = True
                    break

            if not found:
                print("deployment not subscribed to upstream changes")
                return False
        except Exception as e:
            print("Couldn't look up project in subscriptions")
            print(e)
            return False

        prev_project_id = self.latest_pipeline.upstream_project_id
        prev_branch = self.latest_pipeline.upstream_branch
        prev_commit_id = self.latest_pipeline.upstream_commit_id

        print(
            f"{prev_project_id=} {prev_branch=} {prev_commit_id=} {project_id=} {branch=} {commit=}"
        )

        if prev_project_id is None or prev_project_id != project_id:
            print("prevented incremental deploy based on project id")
            return False

        # Allow deployments with no defined branch to incremental deploy
        if prev_branch != branch and prev_branch is not None:
            print("prevented incremental deploy based on branch")
            return False

        if commit is None:
            return True

        if commit == prev_commit_id:
            print("prevented incremental deploy, deployment is up to date")
            return False

        return True

    @cached_property
    def latest_pipeline(self):
        try:
            return UnfurlCloudPipeline(self.deployment_record["pipelines"][-1])
        except (IndexError, KeyError):
            # None if missing pipelines key or empty
            return None

    @property
    def deploy_environment(self):
        try:
            return self.latest_pipeline.deploy_environment
        except:
            return None

    """
  @property
  def latest_digest(self):
    try:
      assert self.latest_pipeline is not None
      return self.latest_digest
  """


class UnfurlCloudPipeline:
    def __init__(self, pipeline_record):
        self.pipeline_record = pipeline_record

    def container_repository_is_ready(self):
        repository_id = f"{os.getenv('UPSTREAM_REPO').lower()}/{os.getenv('UPSTREAM_REF')}"
        repository_id_with_tag = f"{repository_id}:{os.getenv('UPSTREAM_COMMIT')}"

        registry = os.getenv("CI_REGISTRY")

        try:
            docker_client.images.get_registry_data(
                f"{registry}/{repository_id_with_tag}")
            return True
        except docker.errors.APIError as e:
            print(e)
            return False

    async def wait_for_container_available(self):
        if self.container_repository_is_ready():
            return

        print("image is not yet available")
        await asyncio.sleep(10)
        await self.wait_for_container_available()

    @property
    def id(self):
        return self.pipeline_record["id"]

    @property
    def variables_map(self):
        return self.pipeline_record["variables"]

    @property
    def upstream_project_id(self):
        return self.pipeline_record.get("upstream_project_id")

    @property
    def upstream_commit_id(self):
        return self.pipeline_record.get("upstream_commit_id")

    @property
    def upstream_branch(self):
        return self.pipeline_record.get("upstream_branch")

    @property
    def workflow(self):
        result = self.variables_map["WORKFLOW"]
        assert result in ["deploy", "undeploy"]

        return result

    @property
    def deploy_environment(self):
        return self.variables_map["DEPLOY_ENVIRONMENT"]

    @property
    def deployment(self):
        return self.variables_map["DEPLOYMENT"]

    @property
    def variables(self):
        return [dict(key=k, value=v) for (k, v) in self.variables_map.items()]

    @property
    def blueprint_project_url(self):
        return self.variables_map["BLUEPRINT_PROJECT_URL"]


async def wait_for_running_pipelines(project, except_for=None, only=None):
    if except_for is not None or only is not None:
        for pipeline in project.pipelines.list(all=True):
            # XXX make sure its not "master" pipeline like this one? source ==
            pipeline_id = pipeline.attributes.get("id")
            should_await_id = (
                pipeline_id == only if only is not None else pipeline_id != except_for
            )
            if pipeline.attributes.get("status") not in ALLOWED_PARALLEL and should_await_id:
                print("waiting for ", pipeline)
                await asyncio.sleep(10)
                await wait_for_running_pipelines(
                    project, except_for=except_for, only=only
                )
                break


def docker_login_if_needed(project, upstream_project):
    username = f"UNFURL_DEPLOY_TOKEN_{project}"
    password = os.getenv(f"_dep_{upstream_project}")

    registry = os.getenv("CI_REGISTRY")

    if password is not None:
        login_res = docker_client.login(username, password, registry=registry)
        print(f"docker login with {username=} -- {login_res=}")


async def trigger_downstream_deployments():
    CI_SERVER_URL = os.getenv("CI_SERVER_URL")
    ACCESS_TOKEN = os.getenv("UNFURL_PROJECT_TOKEN")
    CI_PROJECT_ID = int(os.getenv("CI_PROJECT_ID"))
    UPSTREAM_BRANCH = os.getenv("UPSTREAM_REF")
    UPSTREAM_COMMIT = os.getenv("UPSTREAM_COMMIT")
    UPSTREAM_PIPELINE = os.getenv("UPSTREAM_PIPELINE")
    UPSTREAM_PROJECT = int(os.getenv("UPSTREAM_PROJECT") or -1)
    PIPELINE_ID = int(os.getenv("CI_PIPELINE_ID"))

    if not (CI_SERVER_URL and ACCESS_TOKEN and CI_PROJECT_ID):
        return None

    gitlab = Gitlab(CI_SERVER_URL, private_token=ACCESS_TOKEN)
    project = gitlab.projects.get(CI_PROJECT_ID)

    docker_login_if_needed(CI_PROJECT_ID, UPSTREAM_PROJECT)

    # if pipelines are running, wait for them to finish
    await wait_for_running_pipelines(project, except_for=PIPELINE_ID)

    pipeline_routines = []

    async def trigger(key, deployment):
        # see git log for implementing parallel trigger
        # started with parallel implementation and switched to sequential

        # make sure the container image is showing up
        await deployment.latest_pipeline.wait_for_container_available()
        # run one deployment at a time
        await wait_for_running_pipelines(project, except_for=PIPELINE_ID)

        pipeline_id = deployment.trigger_incremental_deployment(
            project=project,
            dispatched_by=PIPELINE_ID,
            upstream_pipeline=UPSTREAM_PIPELINE,
            upstream_branch=UPSTREAM_BRANCH,
            upstream_commit=UPSTREAM_COMMIT,
        )

        print("Triggered", key, pipeline_id)

    for key, deployment_record in get_deploymentpaths(unfurl_project).items():
        try:
            deployment = UnfurlCloudDeployment(key, deployment_record)
            if deployment.should_incremental_deploy(
                UPSTREAM_PROJECT, UPSTREAM_BRANCH, UPSTREAM_COMMIT
            ):
                print("Queuing", key, "for incremental deployment")
                pipeline_routines.append(trigger(key, deployment))

        except Exception as e:
            print(f"Something went wrong while attempting to trigger {key}")
            raise e

    await asyncio.gather(*pipeline_routines)


if __name__ == "__main__":
    asyncio.run(trigger_downstream_deployments())
