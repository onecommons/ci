This project contains the CI files used by your dashboard and testbed projects at [Unfurl Cloud](https://unfurl.cloud).

To set it manually, visit your project's `Settings | CI` page and enter `ci/dashboard-gitlab-ci.yml@onecommons/ci` in the "CI/CD configuration file" setting.

The canonical home for this project is at https://unfurl.cloud/onecommons/ci.
